"""Split files based on columns and write parquet files

@author: Karthik Gomadam Rajagopal
@date: December 5, 2015.

Summary:
This script takes an file  and splits it into
different files based on group_id and subgroup_id combination. The
result files are written as parquet. We chose parquet since schema
can be self contained and parquet can also be loaded again into
Spark for compute purposes.

TODO:
1. Go from a single script to a class
2. Schema transforms are hard coded. Make them readable from yaml or
preferably Google Protobuffer. This will make the scripts flexible.
3. Spark configs are hard coded. System level confs should be set
in Spark conf and app confs should set in app conf.
THE CODE IS PEP-8 COMPLIANT
"""
# PYTHON IMPORTS
import sys
import os
from os import path
import datetime
import shutil
from settings import settings
# Performance imports
# from perf.timer import Timer
from applogger.logger import Logger
# from dateutil.parser import parse as date_parser
# SPARK IMPORTS
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.types import StringType
from pyspark.sql.types import StructType
from pyspark.sql.types import StructField
from py4j.protocol import Py4JJavaError

# import master_file_jira_rest
from memory_profiler import profile
#from scripts.utils import jira_rest
#from config import settings


class SparkScheduler(object):
    """
    SparkScheduler class schedules a new split Spark job
    """

    @profile
    def init_and_split(self, **kwargs):
        """
        Returns a new instance of the scheduler.
        Args:
            **kwargs (keyword args): Keyword args to override default
        master and spark confs and strategy to be executed by this scheduler

        Returns:

        """
        try:
            # logger (applogger.logger.Logger)
            self.logger = Logger("SparkScheduler", kwargs["log"])
            # self.strategy (parsers.config_json_parser.Strategy)
            self.strategy = kwargs["strategy"]
            # even though we are in a try except block, we still check for
            # app_name and master because they are not mandatory. We dont
            # want a key error raised because they are not sent over
            # app_name (str)
            app_name = kwargs["app_name"] if "app_name" in kwargs else None
            # master (str)
            master = kwargs["master"] if kwargs["master"] else None
            # self.input_file (str)
            self.input_file = kwargs["input_file"]
            # self.input_file_parts (list)
            self.input_file_parts = self.input_file.split('/')[-1].split('.')
            # self.input_file_name (str)
            self.input_file_name = '.'.join(self.input_file_parts[:-1])
            # self.input_file_ext (str)
            self.input_file_ext = self.input_file_parts[-1]
            self.is_success = False # Only used for JIRA to create the correct JIRA PROJECT
        except KeyError:
            self.logger.critical("Missing keyword parameter {0}. "
                                 "Cannot create scheduler. Exiting")
            sys.exit(1)
        try:
            # self.spark_context (SparkConf)
            # self.sql_context (SQLContext)
            self.spark_context, self.sql_context =\
                    self.init_spark_contexts(self.logger, app_name, master)
            self.logger.debug(self.sql_context)
            # self.dataframe (DataFrame)
            # self.dataframe = None
            # self.completed_files (dict)
            self.completed_files = {}
            self.quality_metric_details = {}
        except Exception:
            print sys.exc_info()
            raise
        return self.split_file(kwargs["job_id"],kwargs["result_queue"])

    def init_spark_contexts(self, ls, logger, app_name=None, master=None):
        """
        Returns the spark and sql contexts for a scheduler
        Args:
            logger (logger.Logger) - The logger to be used for logging
            app_name (str): the app name associated with the context
            master (str): the spark master associated with the context

        Returns:
            (SparkContext, SQLContext) - the spark and sql context for this
        scheduler
        """
        spark_conf = SparkConf()
        spark_conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
        if master:
            spark_conf.setMaster(master)
        if app_name:
            spark_conf.setAppName(app_name)
        spark_context = SparkContext(conf=spark_conf)
        return spark_context, SQLContext(spark_context)

    def load_input_file(self):
        """
        Returns a RDD by loading the given file as a text file
        Args:

        Returns:

        """
        return self.spark_context.textFile(self.input_file)

    def create_schema(self, input_rdd):
        """
        Returns a schema object to be used in dataframe creation by
        splitting the first line on ",". The columns names in schema are
        COL{%d}, where %d is the position of a column in the schema. For
        example, if the file has 10 columns, the column names will go from
        COL1 through 10. Note that this index is different from the zero index
        :/String


        used by python itself. Also, all fields will be of StringType in
        Spark datastructure.

        Args:
            input_rdd: The rdd from which the first will be read

        Returns:
            (StructType): containing the schema for this file
        """
        # header_row (str)
        header_row = input_rdd.first()
        if self.strategy.delimiter == ',':
            # schema_fields (list(StructField))
            return StructType([StructField("COL{0}".format(i+1),
                                       StringType(),
                                       True)
                           for i in xrange(len(header_row.split(",")))])
        else:
            # schema_fields (list(StructField))
            return StructType([StructField("COL{0}".format(i+1),
                                       StringType(),
                                       True)
                           for i in xrange(len(header_row.split("\t")))])

    def create_data_frame(self, input_rdd, schema):
        """
        Creates and returns a dataframe using the RDD from the input
        file and the schema. Uses the sql_context associated with
        this instance.
        Args:
            input_rdd (RDD): The RDD created from the input file
            schema (StructType): The schema generated from the input file

        Returns:
            (DataFrame): Dataframe contaning the input data
        """
        if self.strategy.delimiter == ',':
            file_line = input_rdd.map(lambda il:
                                  [attr.strip('"\'') for attr in il.split(",")])
        else:
            file_line = input_rdd.map(lambda il:
                                  [attr.strip('"\'') for attr in il.split("\t")])
        self.completed_files["original"] = (file_line.count(), None)
        return self.sql_context.createDataFrame(file_line, schema)

    # @profile
    def execute_split_query(self, split_query):
        """

        Args:
            split_query (str): The query to generate the split for a strategy

        Returns:
            (DataFrame): a new RDD generated with the records for the current
        split
        """
        return self.sql_context.sql(split_query)

    # @profile
    def write_to_csv(self, split_rdd, data_home, file_name):
        """
        Writes a given RDD to a CSV file. Checks if the RDD has any records.
        If the RDD does not have any records, will not produce a CSV file
        Args:
            split_rdd (RDD) - The RDD to be serialized
            data_home (str) - The parent directory to write the file to
            file_name (str) - The FQ name of the file to be serialized
        Return:

        """
        # dest_path (str) -  Destination path for split file
        dest_path = '/'.join(file_name.split('/')[:-1])
        # complete_file_name (str) - Master file name + split file name
        complete_file_name = self.input_file_name + file_name.split('/')[-1]
        # fq_file_name (str) - Fully qualified file to write the CSV files to
        fq_file_name = data_home + "/" + complete_file_name
        # line_count (int)
        line_count = split_rdd.count()
        self.logger.debug(line_count)
        if line_count > 0:
            if self.strategy.delimiter == ',':
                split_records = split_rdd.map(
                    lambda data: ','.join(el for el in data)
                )
            else:
                split_records = split_rdd.map(
                    lambda data: '\t'.join(el for el in data)
                )

            try:
                split_records.coalesce(1).saveAsTextFile(fq_file_name)
            except Py4JJavaError:
                self.logger.critical("File {0} exists. Skipping".
                                     format(file_name))
            self.rename_split_file(data_home, complete_file_name, line_count, dest_path)

    def rename_split_file(self, data_home, file_name, linecount, dest_path):
        """
        Renames the split file to employer_name.csv. Employer name
        is the last token when file_name is split by "/". The
        file produced is stored as part-0000 under file_name dir.

        Args:
            data_home (str): The parent path to write the renamed file to
            file_name (str): The directory of to which spark has written the
             split file
            linecount (int): The total number of lines in the generated file
        Returns:

        """
        self.logger.debug(data_home +"/" + file_name)
        # file_path (str) - The fully qualified path of this file
        file_path = data_home +"/" + file_name 
        output_file = file_path + ".txt" 
        try:
            os.rename(file_path + "/" + "/part-00000", output_file)
            self.completed_files[file_name] = (linecount, file_name + ".txt",
                                                    data_home, dest_path)
        except OSError:
            self.logger.critical("Split file not found. Exiting")
            sys.exit(1)
        try:
            if os.path.isdir(file_path):
                shutil.rmtree(file_path)
            else:
                os.remove(file_path)
        except OSError:
            self.logger.log("Could not delete redundant files")

    @profile
    def split_file(self, job_id, result_queue):
        """
        The main method that splits the given file
        """
        self.logger.debug("Calling input rdd from " + self.input_file)
        input_rdd = self.load_input_file()
        self.logger.debug("Created input rdd from " + self.input_file)
        self.logger.debug("Creating schema from input RDD")
        schema = self.create_schema(input_rdd)
        self.logger.debug(schema)
        self.logger.debug("Creating data frame")
        # dataframe (DataFrame)
        dataframe = self.create_data_frame(input_rdd, schema)
        # table_name (str)
        table_name = self.strategy.name
        dataframe.registerTempTable(table_name)
        self.sql_context.cacheTable(table_name)
        self.logger.debug("Created data frame")
        queries = []
        time_suffix = datetime.datetime.utcnow().strftime("%d%H%M%S")
        data_home = SparkScheduler.create_output_directory(self.logger, job_id, time_suffix)
        for splitter in self.strategy.splitters:
            query = "SELECT * FROM {0} WHERE {1}".format(table_name,
                                                         splitter.query)
            self.logger.debug(query)
            # split_rdd (RDD)
            split_rdd = self.execute_split_query(query)
            self.write_to_csv(split_rdd,
                              data_home,
                              "{0}T{1}".format(
                                  splitter.file_prefix,
                                  time_suffix))
            queries.append("( " + splitter.query + ")")
        bad_files_query = " OR ".join(queries)
        bad_files_query = " NOT ({0}) ".format(bad_files_query)
        query = "SELECT * FROM {0} WHERE {1}".format(table_name,
                                                     bad_files_query)
        self.logger.debug(query)
        try:
            # orig_count (int)
            orig_count = self.completed_files["original"][0]
            # split_count (int)
            split_count = 0
            for comp_file in self.completed_files:
                split_count += self.completed_files[comp_file][0]
            self.logger.debug(orig_count)
            self.logger.debug(split_count)
            assert orig_count == split_count - orig_count
            self.completed_files.pop('original', None)
            result_queue.put((self.completed_files,
                              self.quality_metric_details))
        except KeyError:
            self.logger.critical("Original count not set. Exiting")
            return None
        except AssertionError:
            self.logger.critical("Output records do not match with input.")
            return None

    @classmethod
    def create_output_directory(cls, logger, job_id, time_suffix):
        """
        Creates an output parent directory for this job
        The directory is created inside settings.DATA_HOME
        
        Args:
            logger (logger.Logger) - The log handler to use
            job_id (str) : The job_id of the current job
            time_suffix (str): Current time formatted as mmDDHHMMSS 
            to be appended to the directory name

        Returns:
            (bool): True if directory is created or already exists
        """
        try:
            data_home = "{data_home}/{job_id}T{time_suffix}".format(
                        data_home=settings.DATA_HOME,
                        job_id=job_id,time_suffix=time_suffix)
            os.makedirs(data_home)
        except OSError:
            if not os.path.isdir(path):
                logger.critical("Cannot create output directory")
                logger.critical(sys.exc_info())
                return None
        return data_home 
# @profile
def main():
    """
    Voided main method for unit testing purposes
    """
    pass
    # input_file = ""
    # try:
    #     input_file = argv[0]
    # except IndexError:
    #     print "arguments missing"
    # spark_context, sql_context = init_spark_contexts()
    # # load the input file using spark text file loader and return it as RDD
    # with Timer() as t:
    #     input_rdd = load_input_file(spark_context, input_file)
    #     document_schema = header_op_schema_load(input_rdd)
    #     input_rdd_no_header = clean_header_from_file(input_rdd)
    # print("Context initialized, file sanitized in: {0} ms".format(t.msecs))
    # with Timer() as t:
    #     file_dataframe = create_data_frame(input_rdd_no_header,
    #                                        document_schema, sql_context)
    # print("Data frame created and loaded in: {0} ms".format(t.msecs))
    # with Timer() as t:
    #     file_dataframe.registerTempTable("taxi")
    #     sql_context.cacheTable("taxi")
    #     employer_gids = get_group_id(sql_context)
    #     split_emp_files = {}
    #     offset = 0
    #     for emp_gid in employer_gids:
    #         split_sgid = get_subgroup_id(sql_context, emp_gid.group_id)
    #         for sgid in split_sgid:
    #             emp_recs = get_emp_records_gid_sgid(sql_context,
    #                                                 emp_gid.group_id,
    #                                                 sgid.subgroup_id)
    #             split_emp_files["{0}{1}".format(emp_gid.group_id,
    #                                             sgid.subgroup_id)] = emp_recs
    # print "=> Elapsed time %s ms" % t.msecs
    # with Timer() as print_timer:
    #     for gid_sgid in split_emp_files:
    #         write_to_parquet(split_emp_files[gid_sgid], gid_sgid)
    # print "=> Elapsed time for writing in seconds : %s s" % print_timer.secs


if __name__ == "__main__":
    main()
