"""Example of program with many options using docopt.

Usage:
  master_file_splitter.py [--conf CONF_FILE] [--input INPUT_FILE] [--output_dir
  OUTPUT_DIR]


Options:
  -h --help                  Show this help message and exit
  --version                  Show version and exit
  -c --conf CONF_FILE        The configuration file containing the strategy
  -i --input INPUT_FILE      The input file to be split
  -o --output_dir OUTPUT_DIR The output directory to write the file to

"""
import sys
import os
import random
import string
import subprocess
from os import listdir
from os.path import isfile
from os.path import join
import datetime
import time
from settings import settings
from applogger.logger import Logger
from parsers.config_json_parser import ConfigurationJSONParser
from spark_splitter_functions import SparkScheduler
from docopt import docopt
import yaml
import MySQLdb
from MySQLdb import cursors
from multiprocessing import Process, Queue

LOGGER = Logger("Split master", settings.LOG_HOME)


def execute_job(job_id,
                strategy=None,
                input_file=None,
                config=None):
    """
    Sets up the execution for a given job by adding it to Spark scheduler
    queue.
    Args:
        job_id (str): The job id of the current job
        strategy (str): Name of the strategy to be used for this split
        master (str): Master file to be used for this split
        config (str): The configuration JSON for this job
    """
    #LOGGER.debug(job)
    configurator = ConfigurationJSONParser(config, settings.LOG_HOME)
    configurator.create_strategies()
    configurator.parse_strategies()
    current_strategy = None
    try:
        current_strategy = configurator.strategies[strategy]
    except KeyError:
        LOGGER.critical("Strategy {0} not found in config file at {1}. "
                        "Will exit.".format(strategy, config))
        sys.exit(1)
    spark_scheduler = SparkScheduler(
    )

    # self.input_file_parts (list)
    input_file_parts = input_file.split('/')[-1].split('.')
    # self.input_file_name (str)
    input_file_name = '.'.join(input_file_parts[:-1])
    # self.input_file_ext (str)
    #self.input_file_ext = self.input_file_parts[-1]
    #completed_files (list)
    completed_files = []
    # We are using multiprocessing to spin Spark into a separate process. 
    # The results will be written into a queue from which we will read 
    # completed_files and quality metric
    result_queue = Queue()
    splitter_args = {"log": settings.LOG_HOME,
                     "strategy": current_strategy,
                     "input_file": input_file,
                     "master": settings.SPARK_MASTER,
                     "job_id": job_id,
                     "result_queue": result_queue,}
    split_process = Process(target=spark_scheduler.init_and_split,
                            kwargs=splitter_args)
    split_process.start()
    print "Split process for job id: {job_id} "\
          "started with pid: {pid}".format(job_id=job_id,
           pid=split_process.pid)
    completed_files,quality_metric = result_queue.get()
    split_process.join()
    return split_files, error_file, error_msg,summary,quality_metrics


def master_file_pre_process(**kwargs):
    """
    The master_file_pre_process function takes in keyword arguments listed
    below:
       1. job_files: A dictionary that contains  a collection of jobs for
       processing. Each object in the collection has a file config ID, file ID, and
       local file path. Example is:
       {"job_files":[{"file_config_id":1,
                      "file_id":2,
                      "local_file_path":"/foo/test.csv",
                      "output_file_path":"/output_path",
                       ], }, "job_id": 100, "job_params": {}, }
       2. On successful execution, the function will return a dictionary in the
       following format:
       {"status": <One of True or False on success or failure>,
        "status_summary": An optional message, usually a log snippet,
        "error_msg": In case of a failure, the last exc_info,
        "jira_ticket": The ticket associated with this split job,
        "sub_file_count": The number of subfiles produced,
        "sub_file_names":[list of generated files, ],
        "error_file_name": name of the error file,
       }
       The function is the main controller for the spark jobs. The description
       will be updated as we continye to develop.
    """
    try:
        LOGGER.debug(kwargs)
        job_params = kwargs["job_params"]
        LOGGER.debug(job_params)
        config_file = job_params["json_file"]
        job_files = kwargs["job_files"]
        strategy = job_params["strategy_name"]
        """
        job_files: Collection of job descriptions as described in function
        document
        """
        for job in job_files:
            input_file = '/home/sftpuser/CFF_test/upload' + job["local_file_path"]
            input_file = extract_data_file(input_file, 'tmp')
            split_files, error_file, error_msg, summary,quality_metric = execute_job(
                job_id=kwargs["job_id"],
                strategy=strategy,
                input_file=input_file,
                config=config_file)
            try:
                return {"status": 'Success',
                        "status_summary": summary,
                        "error_msg": None,
                        "jira_ticket": None,
                        "sub_file_count": len(split_files),
                        "sub_file_names": split_files,
                        "error_file": error_file,
                        "quality_metrics": quality_metric,
                        }
            except TypeError:
                return {"status": 'Failed',
                        "status_summary": "Stub output",
                        "error_msg": error_msg,
                        "jira_ticket": None,
                        "sub_file_count": 0,
                        "sub_file_names": [],
                        "error_file": error_file,
                        "quality_metrics": [],
                        }
    except KeyError, k:
        LOGGER.critical("Critical error parsing argumemts {0}".format(k))
    
def main(arguments):
    """
    Main entry point for the script. Also used in testing.
    Args:
        arguments (dict): the command line arguments if given for the
            script

    Returns:

    """
    test_job_dict = {u'job_files': [{u'file_config_id': 1592,
                                     u'file_id': 13,
                                     u'local_file_path':
                                         u'/TEST_Castlight_20160322000000_20160322235959_20160630083607_1of1.txt.gz'}
                                   ],
                     u'job_params': {u'strategy_name': u'Anthem_master_file_split_strategy',
                                     u'json_file': u'/share/whdata/backup/phi/splitting_config_json.txt'
                                    },
                     u'job_id': 13}     
    print master_file_pre_process(**test_job_dict)


if __name__ == '__main__':
    args = docopt(__doc__, version='0.0.0dv1')
    main(args)
