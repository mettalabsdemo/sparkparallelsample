"""
Contains parser for parsing the configuration JSON file
"""
import os
import json
import sys
import inspect
import time
#sys.path.append("/work/code/cff_master/scripts/utils/master_file_splitter/")
sys.path.append("/cff_master/scripts/utils/master_file_splitter/")
from applogger.logger import Logger


class ConfigurationJSONParser(object):
    """
    Configuration JSON parser. Each file is parsed into a parser object.
    Each parser object contains different strategy objects for splitting.
    """

    def __init__(self, json_file, log_root):
        """
        Constructor for returning an instance of the Config parser
        Args:
            json_file (str): Fully qualified path to the json file

        Returns:
            (ConfigurationJSONParser) - instance of the Config parser
        """
        # self.json_file (str)
        self.json_file = json_file
        # self.parsed_file (list(dict)) - list of strategy configs in
        # this file
        self.parsed_file = self.load_config_json()
        # self.strategies (dict) - list of strategies in this conf
        self.strategies = {}
        # self.log_root (str)
        self.log_root = log_root
        # self.logger (applogger.logger.Logger)
        self.logger = Logger("ConfigurationJsonParser{0}".format(time.time()), self.log_root)

    def load_config_json(self):
        """
        Loads the config JSON and returns a list of the strategies.

        Args:
            json_file (str) - Fully qualified file name of the config
        Returns:
            (list(dict)) - strategies in the config
        """
        with open(self.json_file) as config_file:
            return json.load(config_file)

    def create_strategies(self):
        """
        Given a collection of strategies, parse them and
        create split queries. The queries are stored in
        the Strategy object.

        Args:

        Returns:

        """
        self.logger.debug("inside cs")
        for strategy_dict in self.parsed_file:
            try:
                self.strategies[strategy_dict["name"]] = Strategy(
                            strategy_expression=strategy_dict["expression"],
                            strategy_name=strategy_dict["name"],
                            fallback_file=strategy_dict["fallback_file"],
                            fallback_file_path=strategy_dict["fallback_file_path"],
                            delimiter=strategy_dict["delimiter"].decode("string_escape"),
                            header=strategy_dict['header'],
                            footer=strategy_dict['footer'],
                            subfiles=strategy_dict["subfiles"],
                            log_root=self.log_root
                            )
                self.logger.debug("Length of strategies"\
                        " {0}".format(len(self.strategies)))
                self.logger.debug("*"*20)
            except KeyError, k:
                self.logger.critical("Error loading strategy {0}".format(k))
                raise k

    def parse_strategies(self):
        """
        Parse the strategies in the current config file and populate the
        query object for each splitter in the subfiles.
        Returns:

        """
        # self.logger.debug("Parsing {0} strategies".format(
        #    len(self.strategies)))
        for strategy in self.strategies.itervalues():
            # parse the expression in the strategy
            # self.logger.debug("Parsing expression")
            strategy.parse_expression()
            # self.logger.debug("Splitting strategy")
            strategy.create_splitters()
            # self.logger.debug("Generating query")
            strategy.generate_queries()


class Strategy(object):
    """
    Class storing a strategy from the config json file
    """

    def __init__(self,
                 strategy_name, fallback_file, fallback_file_path,
                 delimiter, header, footer, strategy_expression, subfiles,
                 log_root):
        """
        Returns a Strategy object with a given name, fallback file, and a strategy expression.
        Args:
            strategy_name (str): The name of the strategy
            fallback_file (str): File name to store bad claims
            fallback_file_path (str): Path of file for bad claims
            delimiter (str): Column delimiter for input file
            header (int): 1 if header is present else 0
            footer (int): 1 if footer is present else 0
            strategy_expression (str): Expression to split claims
            source_file (str): The fully qualified path of the source file
        associated with this stragegy
            log_root (str): The location of log files for the logger
        Returns:
            (Strategy): Strategy object
        """
        # check if we can read the source file. if not raise IOError and exit
        # self.log_root (str)
        self.log_root = log_root
        # self.logger (applogger.logger.Logger)
        self.logger = Logger("Strategy{0}".format(time.time()), self.log_root)
        # if not os.access(source_file, os.R_OK):
        #     self.logger.critical("Cannot access file {0}.".format(source_file))
        #     raise IOError
        # self.name (str)
        self.name = strategy_name
        # self.fallback_file (str)
        self.fallback_file = fallback_file
        # self.fallback_file_path (str)
        self.fallback_file_path = fallback_file_path
        # self.delimiter (str)
        self.delimiter = delimiter
        # self.header (int)
        self.header = header
        # self.footer (int)
        self.footer = footer
        # self.expression (str)
        self.expression = strategy_expression
        # self.subfiles (list(dict))
        self.subfiles = subfiles
        # self.parse_tree (Strategy.ExpressionParseTree)
        self.parse_tree = None
        # self.splitters (list(Strategy.Splitter))
        self.splitters = []

    def parse_expression(self):
        """
        Parses the expression of this strategy object and assigns to
        the parse_tree
        Args:


        Returns:

        """
        # ParseTree (Strategy.ExpressionParseTree): Static type shortcut
        parsetree = Strategy.ExpressionParseTree
        # exp_tokens (list(str)) 
        exp_tokens = [token.strip() for token in self.expression.split(" ")]
        # is_var_name (bool): Flag indicating if current token is a variable
        is_var_name = True
        # token_name (str): The name of the current node being parsed
        token_name = ''
        # current_tree (ParseTree) - the current subtree being constructed
        current_tree = []
        for token in exp_tokens:
            if token in Strategy.Splitter.COMPARATORS:
                is_var_name = False
            elif token in Strategy.Splitter.OPERATORS:
                current_tree.append(parsetree(node_name=token))
                is_var_name = True
            else:
                if is_var_name:
                    current_tree.append(parsetree(node_name=token))
            if len(current_tree) == 3:
                current_tree[1].add_child(current_tree[0], "left")
                current_tree[1].add_child(current_tree[2], "right")
                current_tree[0] = current_tree[1]
                del(current_tree[1:])
        # check if the current tree has more than one element here.
        # if it does, shrink it
        try:
            current_tree[1].add_child(current_tree[0], "left")
            current_tree[0] = current_tree[1]
        except IndexError:
            pass
        self.parse_tree = current_tree[0]

    def create_splitters(self, ignore_keys=None):
        """
        Parse the subfile records and create splitter lists for each
        subfile. Each element in the splitter dict is a dict whose
        key is the group_key and whose value is the group_val. For
        example, if a subfile record is
        {group_key: foo, group_val: bar, a:b, c:d}, the corresponding
        entry in the splitter list will be:
        [{foo: a, bar: b}, {foo: c, bar: d}, ]. If there are nested cases,
        this method will need to be changed.

        Args:
            ignore_keys: List of keys to ignore when processing the sublist.
            Usually these are group_key, group_val, and subfile_prefix.
        """
        # ignore_keys (list(str))
        ignore_keys = ["group_key",
                       "group_val",
                       "sub_file_prefix",
                       "destination_path", ]\
                       if not ignore_keys else ignore_keys
        for subfile in self.subfiles:
            splitter_key = subfile["group_key"]
            if 'group_val' in subfile:
                splitter_val = subfile["group_val"]
            splitter = Strategy.Splitter(
                subfile["destination_path"] + "/" +  subfile["sub_file_prefix"],
                log_root=self.log_root
            )
            # is_valid_splitter (bool)
            is_valid_splitter = True
            for gkey in subfile:
                if gkey not in ignore_keys:
                    try:
                        if subfile[gkey]:
                            splitter.split_list.append(
                                {splitter_key: gkey,
                                 splitter_val: subfile[gkey], }
                            )
                        else:
                            splitter.split_list.append(
                                {splitter_key: gkey,
                                 }
                            )
                    except KeyError:
                        is_valid_splitter = False
                        raise KeyError
            if is_valid_splitter:
                self.splitters.append(splitter)
        return True

    def generate_queries(self):
        """
        Iterates through the splitters and generates the query for
        each splitter by calling the splitter's construct query method

        Returns:
            list((str)) - the query for the splitter
        """
        # queries (list)
        queries = []
        '''
        self.logger.debug("{0} calls generate_query".format(
            inspect.stack()[1][3]
        ))
        '''
        expression = self.parse_tree.inorder()
        # self.logger.debug(len(self.splitters))
        for splitter in self.splitters:
            # expression (list)
            splitter.construct_query(expression)
            self.logger.debug(splitter)

    def __repr__(self):
        return "Strategy: {0}, Fallback file: {1}," \
               "Fallback file path: {2}, Splitters: {3}".format(
                self.name, self.fallback_file,
                self.fallback_file_path, self.delimiter,
                self.header, self.footer,
                self.splitters
                )

    class Splitter(object):
        """
        Models the strategy for each subfile in the strategy file
        """
        # comparators (list(str)) - A list of comparison operators
        # currently supported in the expression parser
        COMPARATORS = ["="]
        # operators (list(str)) - A list of operators currently
        # supported in the expression parser
        OPERATORS = ["AND", "OR", ]

        def __init__(self, file_prefix, log_root):
            """
            Returns a strategy object
            Args:
                file_prefix (str): The fully qualified file name to write the file
                produced by this split

            Returns:
                (Strategy) object
            """
            # self.log_root
            self.log_root = log_root
            # self.file_prefix (str)
            self.file_prefix = file_prefix
            # self.query (str)
            self.query = None
            # self.split_list (list(dict))
            self.split_list = []
            # self.logger (applogger.logger.Logger)
            self.logger = Logger("Strategy.Splitter{0}".format(time.time()), self.log_root)

        def construct_query(self, expression):
            """
            Returns the query to be run against Spark data frame to
            generate this split

            Args:
                expression (list): The parse tree of the query expression
                as a list

            Returns:
                (str) - The query to be run against Spark dataframe to produce
                this split.

            """
            # queries (list)
            queries = []
            # local_copy_expression (list)
            for split in self.split_list:
                local_expression = list(expression)
                for token_pos, expression_token in enumerate(expression):
                    try:
                        if expression_token in split:
                                if not isinstance(split[expression_token], list):
                                    local_expression[token_pos] = expression_token +\
                                        "=\"" + split[expression_token] + '"'
                                else:
                                    local_expression[token_pos] = Strategy.Splitter.\
                                        __construct_disjunction(
                                        expression_token,
                                        split[expression_token])
                    except KeyError, k:
                        self.logger.critical("Error constructing query: "
                                             "{0}".format(k))
                        raise KeyError
                queries.append(local_expression)
            # reduced_query (list)
            reduced_query = []
            # query (list)
            for query in queries:
                # check if the value is a list. if it is, combine using
                # paranthesis or expanded disjunction depending on spark
                # behavior
                reduced_query.append("(" +
                                     " ".join(query[:-1]) +
                                     " (" + query[-1] + "))")
            self.query = " or " .join(reduced_query)


        @staticmethod
        def __construct_(operator, variable, values):
            """
            Given a variable and a list of values, returns a string
            that is a combined using the given operator of expressions of
            the form variable = value
            Args:
                operator (str): The operator to combine the values with
                variable (str): variable to be used in expression
                values (list): List of possible values

            Returns:
                (str): a string with the operator combined statement
            """
            # variable_name (str)
            variable_name = "{0} = ".format(variable)
            try:
                # statement (str)
                # move this quote to variable name via format
                statement = operator.join([variable_name + '"' + value + '"'
                                           for value in values])
            except TypeError:
                statement = operator.join([variable_name + '"'+str(val) + '"'
                                           for val in values])
            return statement

        @staticmethod
        def __construct_conjunction(variable, values):
            """
            Given a variable and a list of values, returns a string
            that is a conjunction of expressions of the form
            variable = value. Calls __construct_ method and returns value
            from that
            Args:
                variable (str): The variable to be used in conjunction
                values (list): List of possible values

            Returns:

            """
            return Strategy.Splitter.__construct_(" AND ",
                                                  variable,
                                                  values)

        @staticmethod
        def __construct_disjunction(variable, values):
            """
            Given a variable and a list of values, returns a string
            that is a disjunction of expressions of the form
            variable = value. Calls __construct_ method and returns value
            from that
            Args:
                variable (str): The variable to be used in disjunction
                values (list): List of possible values

            Returns:

            """
            return Strategy.Splitter.__construct_(" OR ",
                                                  variable,
                                                  values)

        def __repr__(self):
            # return "Spliting lists {0} and writing to {1} using query {2}".\
            #    format(self.split_list, self.file_prefix, self.query)
             return "query {0}".\
                format(self.query)

    class ExpressionParseTree(object):
        """
        Class to store the create and store the parsed expression
        as a Tree
        """
        # NODE (dict)
        NODE = {"node_name": None,
                "left": None,
                "right": None, }

        def __init__(self,
                     node_name,
                     left=None,
                     right=None
                     ):
            """

            Args:
                node_name (str): The name of the node
                left (str or ExpressionParseTree) - Left subtree
                right (str or ExpressionParseTree) - Right subtree
            Returns a new instance of the tree. Initializes
            left and right subtrees.
            Returns:
                (ExpressionParseTree) - Instance of the Tree
            """
            # self.root_node (dict) - a copy of the static Node
            self.node_name = node_name
            self.left = None
            self.right = None
            self.parent = None

        def set_left(self, node):
            node = Strategy.ExpressionParseTree(node)
            self.left = node
            self.left.parent = self

        def add_child(self, child, position):
            """
            Adds a right child to the tree. If the right child is a string,
            adds it to right node else adds it to right tree

            Args:
                child (str or ExpressionParseTree): The right child to
                be added.
                position (str) - Whether this child should be added as left
                or right child

            Returns:

            """
            if position == "left":
                self.left = child
                child.parent = self
            elif position == "right":
                self.right = child
                child.parent = self

        def inorder(self, nodes=None):
            """
            In order traversal of this tree; returns a list of nodes

            Args:
                nodes (list(str)) - the set of nodes that have been processed
                root (Strategy.ExpressionParseTree) - The tree to be traversed

            Returns:
                (list(str)) - list of nodes
            """
            if nodes is None:
                nodes = []
            if self.left:
                self.left.inorder(nodes)
            nodes.append(self.node_name)
            if self.right:
                self.right.inorder(nodes)
            return nodes

        def postorder(self, nodes=None):
            """
            In order traversal of this tree; returns a list of nodes

            Args:
                nodes (list(str)) - the set of nodes that have been processed
                root (Strategy.ExpressionParseTree) - The tree to be traversed

            Returns:
                (list(str)) - list of nodes
            """
            if nodes is None:
                nodes = []
            if self.left:
                self.left.inorder(nodes)
            if self.right:
                self.right.inorder(nodes)
            nodes.append(self.node_name)
            return nodes

        def __repr__(self):
            """
            Returns the string representation of this tree by recursively
            calling repr on left and right trees
            Returns:
                (str): The string representation of the tree
            """
            # val = ""
            # if self.left:
            #     val += repr(self.left)
            # val += self.node_name
            # if self.right:
            #     val += repr(self.right)
            return " ".join([str(val) for val in self.inorder(nodes=[])])


def main():
    """
    Produces an example JSON that we can use to test the parser
    Returns:

    """
    # config_json (dict) - a dictionary holding the example configuration
    config_json = [{
        "expression": "COL1 = VAL1 AND COL2 = VAL2",
        "fallback_file": "bad_claims",
        "fallback_file_path": "/home/whops/test_dir/Wellpoint/upload/",
        "delimiter": ",",
        "header": 1,
        "footer": 1,
        "name": "Anthem_master_file_split_strategy",
        "subfiles": [
            {"group_key": "COL1",
             "group_val": "COL2",
             "IN008": ["NASCO", ],
             "AB957": ["WGS", ],
             "sub_file_prefix":"_SAFEWAY_",
            },
            {"group_key": "COL1",
             "group_val": "COL2",
             "empgid1": [6, 7, 8, 11, 12, 33, ],
             "empgid2": [6, 7, 8, 9, 11, 33, ],
             "sub_file_prefix":"_WALMART_",
            }
        ], }, ]
    # config_json (str) - The JSON representation of the dictionary. using same name
    config_json = json.dumps(config_json)
    # json_file (File) - File handler to write to file
    with open("json_example.json", "w") as json_file:
        json_file.write(config_json)
    config_parser = ConfigurationJSONParser(os.getcwd()+"/json_example.json")
    config_parser.create_strategies()
    config_parser.parse_strategies()
    parsetree = Strategy.ExpressionParseTree
    tree_builder = Strategy.ExpressionParseTree(1)
    left_tree = parsetree(3)
    left_tree.add_child(parsetree(5), "left")
    print tree_builder.add_child(left_tree, "left")
    print tree_builder.add_child(parsetree(2), "right")
    print tree_builder

if __name__ == "__main__":
    main()
